import pygame

BLANCO=[255,255,255]
VERDE=[0,255,0]
ROJO=[255,0,0]
AZUL=[0,0,255]
NEGRO=[0,0,0]
AMARILLO=[255,255,102]
AZUL_CLARO=[153,204,255]
VERDE_CLARO=[0,200,140]

#Se define como ruta de la imagen y cantidad de sprites
def recortar(img, size):
    fondo = pygame.image.load(img)
    info = fondo.get_rect()
    f_ancho = info[2]
    f_alto = info[3]

    matriz = []
    numsp_ancho = size[0]
    numsp_alto = size[1]
    ancho_sp = int(f_ancho / numsp_ancho)
    alto_sp = int(f_alto / numsp_alto)
    print("ancho ", ancho_sp, "alto ", alto_sp)

    for fil in range(numsp_alto):
        listAux = []

        for col in range(numsp_ancho):
            cuadro = fondo.subsurface(ancho_sp*col,alto_sp*fil,ancho_sp,alto_sp)
            listAux.append(cuadro)

        matriz.append(listAux)

    return matriz

def Pausa(pantalla):
    intro=pygame.mixer.Sound('Sonidos/Pausa.ogg')
    intro.play()
    intro.set_volume(0.4)
    Fuente=pygame.font.Font(None,32)
    msj='Pulsa escape para Reanudar Juego'
    while not False:
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                return True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    intro.play()
                    return False
        texto=Fuente.render(msj,True,BLANCO)
        pantalla.blit(texto, [100,70])
        pygame.display.flip()

def Inicio(pantalla):
    intro=pygame.mixer.Sound('Sonidos/Intro.ogg')
    intro.play(-1)
    intro.set_volume(0.3)
    Fuente=pygame.font.Font(None,32)
    msj='Pulsa aqui para Iniciar Juego'
    msj1='Pulsa aqui para salir'
    y_opcion=260
    opcion=0
    fin = False
    while (not fin):
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                fin=True
            if event.type == pygame.KEYDOWN:
                if (event.key == pygame.K_SPACE) and (opcion==0):
                    intro.stop()
                    print('opcion0')
                    return fin
                if (event.key == pygame.K_SPACE) and (opcion==1):
                    intro.stop()
                    fin=True
                    print('Opccion1')
                    return fin
                if (event.key == pygame.K_DOWN) and (opcion==0):
                    opcion=1
                    y_opcion+=50
                if (event.key == pygame.K_UP) and (opcion==1):
                    opcion=0
                    y_opcion-=50
        pantalla.fill(NEGRO)
        texto=Fuente.render(msj,True,BLANCO)
        texto1=Fuente.render(msj1,True,BLANCO)
        pantalla.blit(texto, [100,250])
        pantalla.blit(texto1, [100,300])
        pygame.draw.circle(pantalla, BLANCO, [75, y_opcion], 7)
        pygame.display.flip()
    pygame.quit()

def LoseGame(pantalla, txt):
    musica = pygame.mixer.Sound('Sonidos/gameover.ogg')
    musica.play()
    musica.set_volume(0.3)
    # Despues del fin de juego
    fuente = pygame.font.Font(None, 34)
    texto = txt
    txt_info = fuente.render(texto, True, BLANCO)
    txt_mensaje = fuente.render('Presiona la tecla Espacio para continuar', True, BLANCO)
    fin = False
    while not fin:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin=True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    fin=True


        pantalla.fill(NEGRO)
        pantalla.blit(txt_info,[400,300])
        pantalla.blit(txt_mensaje, [300,500])
        pygame.display.flip()
    pygame.quit()
