import configparser
from libJuego import *
from nivel01 import *
from nivel02 import *

BLANCO=[255,255,255]
VERDE=[0,255,0]
ROJO=[255,0,0]
AZUL=[0,0,255]
NEGRO=[0,0,0]
AMARILLO=[255,255,102]
AZUL_CLARO=[153,204,255]
VERDE_CLARO=[0,200,140]

ANCHO = 1080
ALTO = 720

def main():
    pygame.init()
    pantalla = pygame.display.set_mode([ANCHO,ALTO])

    nivel = 1
    TXT = ''
    Ps_nvl = False

    fin=False
    finJuego = False
    reloj = pygame.time.Clock()
    while (not fin) and (not finJuego):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin=True
        
        fin = Inicio(pantalla)
        if fin == True:
            nivel = 0

        if nivel == 1:
            finJuego, TXT, Ps_nvl = nivel01(pantalla)
            if Ps_nvl:
                nivel +=1
        if nivel == 2:
            finJuego, TXT, Ps_nvl = nivel02(pantalla)
            if Ps_nvl:
                nivel +=1

    
    if fin == False:
        LoseGame(pantalla, TXT)

    if finJuego == True:
        return main()

    pygame.quit()

if __name__ == '__main__':
    main()
