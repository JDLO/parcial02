import pygame
import random
import configparser
from libJuego import *

BLANCO=[255,255,255]
VERDE=[0,255,0]
ROJO=[255,0,0]
AZUL=[0,0,255]
NEGRO=[0,0,0]
AMARILLO=[255,255,102]
AZUL_CLARO=[153,204,255]
VERDE_CLARO=[0,200,140]

ANCHO = 1080
ALTO = 720

class Jugador(pygame.sprite.Sprite):
    def __init__(self, mAnimation):
        pygame.sprite.Sprite.__init__(self)
        self.m = mAnimation
        self.con = 0
        self.dir = 0
        self.cteHasta = 0
        self.cteDesde = 0
        self.image=self.m[self.dir][self.con]
        self.rect = self.image.get_rect()
        self.rect.x = 600 #posicion x
        self.rect.y = 400
        self.velx = 0 #velocidad x
        self.vely = 0
        self.salud = 100
        self.golpe = False
        self.cartas = 0
        self.sonido = pygame.mixer.Sound('Sonidos/ough.ogg')
        self.bloques = pygame.sprite.Group()

    def update(self):
        self.rect.x += self.velx
        self.rect.y += self.vely

        if self.golpe == True:
            if self.velx > 0:
                self.velx -= 1
            if self.velx < 0:
                self.velx += 1
            if self.vely > 0:
                self.vely -= 1
            if self.vely < 0:
                self.vely += 1
            if self.velx ==0 and self.vely == 0:
                self.golpe = False

        if self.con < self.cteHasta:
            self.con +=1
        else:
            self.con = self.cteDesde 

        self.image=self.m[self.dir][self.con]

        #COLISIONES CON BLOQUES
        lista_Colision = pygame.sprite.spritecollide(self, self.bloques, False)
        if len(lista_Colision) > 0:
            for b in lista_Colision:
                if self.rect.right > b.rect.left and self.velx > 0:
                    print('Colision derecha')
                    self.rect.right = b.rect.left +1
                    self.rect.bottom += -2
                    self.velx = 0
                elif self.rect.left < b.rect.right and self.velx < 0:
                    print('Colision Izquierda')
                    self.rect.left = b.rect.right +1
                    self.velx = 0
                    if self.rect.bottom > b.rect.top:
                        self.rect.bottom += -5
                elif self.rect.top < b.rect.bottom and self.vely < 0:
                    print('Colision Inferior')
                    self.rect.top = b.rect.bottom +2
                    self.vely = 0
                elif self.rect.bottom > b.rect.top and self.vely > 0:
                    print('Colision Superior')
                    self.rect.bottom = b.rect.top+2
                    self.vely = 0

class Bloque(pygame.sprite.Sprite):
    def __init__(self, img, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        self.rect.x = pos[0] #posicion x
        self.rect.y = pos[1]

class Carta(pygame.sprite.Sprite):
    def __init__(self, img, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        self.rect.x = pos[0] #posicion x
        self.rect.y = pos[1]
        self.sonido = pygame.mixer.Sound('Sonidos/takeup.ogg')

class Enemigo(pygame.sprite.Sprite):
    def __init__(self, img, salud, pos):
        pygame.sprite.Sprite.__init__(self)
        self.m = img
        self.con = 0
        self.dir = 0
        self.cteHasta = 0
        self.cteDesde = 0
        self.image=self.m[self.dir][self.con]
        self.rect = self.image.get_rect()
        self.rect.x = pos[0] #posicion x
        self.rect.y = pos[1]
        self.velx = 0 #velocidad x
        self.vely = 0
        self.salud = salud
        self.bloques = pygame.sprite.Group()

    def update(self):
        self.rect.x += self.velx
        self.rect.y += self.vely

        self.image = self.m[self.dir][self.con]
        if self.con < self.cteHasta:
            self.con +=1
        else:
            self.con = self.cteDesde
        if self.velx>0:
            self.dir=2
        if self.velx<0:
            self.dir=1
        if self.vely<0:
            self.dir=3
        if self.vely>0:
            self.dir=0

        if self.con < self.cteHasta:
            self.con += 1
        else:
            self.con = self.cteDesde

        self.image=self.m[self.dir][self.con]

        #COLISIONES CON BLOQUES
        lista_Colision = pygame.sprite.spritecollide(self, self.bloques, False)
        for b in lista_Colision:
            if self.rect.right > b.rect.left and self.velx > 0:
                self.rect.right = b.rect.left
                self.velx = random.randrange(-2, 0)
            if self.rect.left < b.rect.right and self.velx < 0:
                self.rect.left = b.rect.right
                self.velx = random.randrange(0, 2)
            if self.rect.top < b.rect.bottom and self.vely < 0:
                self.rect.top = b.rect.bottom
                self.vely = random.randrange(0, 2)
            if self.rect.bottom > b.rect.top and self.vely > 0:
                self.rect.bottom = b.rect.top
                self.vely = random.randrange(-2, 0)

def nivel01(pantalla):
    musica = pygame.mixer.Sound('Sonidos/ambiente1.ogg')
    musica.play(-1)
    info_mapa = configparser.ConfigParser()
    info_mapa.read('mapaJuego.map')
    nom_archivo = info_mapa.get('informacion', 'archivo')

    fondo_Recortes = pygame.image.load(nom_archivo)
    info = fondo_Recortes.get_rect()
    fRecortes_ancho = info[2]
    fRecortes_alto = info[3]

    #Fondo del juego
    fondo = pygame.image.load('Imagenes/fondo.png')
    info = fondo.get_rect()
    f_ancho = info[2]
    f_alto = info[3]

    matriz = []
    numsp_ancho = int(info_mapa.get('informacion', 'sp_ancho'))
    numsp_alto = int(info_mapa.get('informacion', 'sp_alto'))
    ancho_sp = int(fRecortes_ancho / numsp_ancho)
    alto_sp = int(fRecortes_alto / numsp_alto)

    for fil in range(numsp_alto):
        listAux = []

        for col in range(numsp_ancho):
            cuadro = fondo_Recortes.subsurface(ancho_sp*col,alto_sp*fil,ancho_sp,alto_sp)
            listAux.append(cuadro)

        matriz.append(listAux)
    mapa = info_mapa.get('informacion', 'mapa')
    filas = mapa.split('\n')

    lim_derecho = ANCHO - 500
    lim_izquierdo = 500
    lim_abajo = ALTO - 300
    lim_arriba = 300
    
    jugadores = pygame.sprite.Group()
    bloques = pygame.sprite.Group()
    enemigos = pygame.sprite.Group()
    cartas = pygame.sprite.Group()

    con_fil = 0
    for f in filas:
        con_col = 0
        for col in f:
            if col != '.':
                num_col = int(info_mapa.get(col, 'col'))
                num_fil = int(info_mapa.get(col, 'fil'))
                b=Bloque(matriz[num_fil][num_col], [ancho_sp*con_col, alto_sp*con_fil])
                bloques.add(b)
            con_col += 1
        con_fil += 1

    m = recortar('Imagenes/kate.png', [4, 4])
    j1 = Jugador(m)
    jugadores.add(j1)
    j1.bloques = bloques

    spriteEnemigo = recortar('Imagenes/Personas3.png', [12,8])

    for i in range(7):
        enemy = Enemigo(spriteEnemigo, 200, [300, 500])
        enemy.bloques = bloques
        enemy.vely = random.randrange(-5, 5)
        enemy.velx = random.randrange(-5, 5)
        enemy.cteHasta = 2
        enemigos.add(enemy)

    for i in range(7):
        enemy = Enemigo(spriteEnemigo, 200, [2010, 800])
        enemy.bloques = bloques
        enemy.vely = random.randrange(-5, 5)
        enemy.velx = random.randrange(-5, 5)
        enemy.cteHasta = 2
        enemigos.add(enemy)

    for i in range(7):
        enemy = Enemigo(spriteEnemigo, 200, [2000, 500])
        enemy.bloques = bloques
        enemy.vely = random.randrange(-5, 5)
        enemy.velx = random.randrange(-5, 5)
        enemy.cteHasta = 8
        enemy.cteDesde = 6
        enemigos.add(enemy)

    for i in range(7):
        enemy = Enemigo(spriteEnemigo, 200, [3000, 2000])
        enemy.bloques = bloques
        enemy.vely = random.randrange(-5, 5)
        enemy.velx = random.randrange(-5, 5)
        enemy.cteHasta = 8
        enemy.cteDesde = 6
        enemigos.add(enemy)

    spriteCarta = recortar('Imagenes/modificadores03.png', [16,16])
    pos_carta = [[928,1856], [992,896], [2944,1024],  [3008, 1504], [2080,2080]]
    for i in range(len(pos_carta)):    
        carta = Carta(spriteCarta[9][4], pos_carta[i])
        cartas.add(carta)
    

    f_posx = 0
    f_posy = 0
    f_velx = -10
    f_vely = -10

    f_limite_der = ANCHO - (f_ancho+380)
    f_limite_izq = 380
    f_limite_arr = 380
    f_limite_aba = ALTO - (f_alto+380)

    finJuego =False
    fin=False
    reloj = pygame.time.Clock()
    while (not fin) and (not finJuego):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin=True
                musica.stop()

            
            if  event.type == pygame.KEYDOWN:
                j1.velx = 0
                j1.vely = 0
                j1.golpe = False
                #DERECHA
                if event.key == pygame.K_RIGHT:
                    j1.velx = 10
                    j1.dir = 2
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  3
                #IZQUIERDA
                if event.key == pygame.K_LEFT:
                    j1.velx = -10
                    j1.dir = 1
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  3
                #ARRIBA
                if event.key == pygame.K_UP:
                    j1.vely = -10
                    j1.dir = 3
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  3
                #ABAJO
                if event.key == pygame.K_DOWN:
                    j1.vely = 10
                    j1.dir = 0
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  3

                if event.key == pygame.K_ESCAPE:
                    musica.stop()
                    Pausa(pantalla)
                    musica.play(-1)

            if event.type == pygame.KEYUP:
                j1.velx = 0
                j1.vely = 0
                #DERECHA
                if event.key == pygame.K_RIGHT:
                    j1.velx = 0
                    j1.dir = 2
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  0
                #IZQUIERDA
                if event.key == pygame.K_LEFT:
                    j1.velx = 0
                    j1.dir = 1
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  0
                #ARRIBA
                if event.key == pygame.K_UP:
                    j1.vely = 0
                    j1.dir = 3
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  0
                #ABAJO
                if event.key == pygame.K_DOWN:
                    j1.vely = 0
                    j1.dir = 0
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  0
        
        #Colision contra enemigos 
        lis_Col = pygame.sprite.spritecollide(j1, enemigos, False)
        if len(lis_Col) > 0:
            j1.golpe = True
            col = lis_Col[-1]
            if (j1.rect.bottom > col.rect.bottom - 20 and j1.rect.bottom < col.rect.bottom + 20) and j1.velx > 0:
                print("Colision Izquierda")
                j1.rect.right = col.rect.left +1
                j1.velx = -10
                j1.salud -= 10
                j1.sonido.play()
            elif (j1.rect.bottom > col.rect.bottom - 20 and j1.rect.bottom < col.rect.bottom + 20) and j1.velx < 0:
                print('Colision Derecha')
                j1.rect.left = col.rect.right +1
                j1.velx = 10
                j1.salud -= 10
                j1.sonido.play()
            elif j1.rect.top < col.rect.bottom -20 and j1.vely < 0:
                print('Colision Inferior')
                j1.rect.top = col.rect.bottom +1
                j1.vely = 10
                j1.salud -= 10
                j1.sonido.play()
            elif (j1.rect.bottom > col.rect.bottom -20 ) and j1.vely > 0:
                print('Colision Superior')
                j1.rect.bottom = col.rect.bottom -21
                j1.vely = -10
                j1.salud -= 10
                j1.sonido.play()
            elif (j1.rect.bottom > col.rect.bottom - 20 and j1.rect.bottom < col.rect.bottom + 20) and (j1.rect.top < col.rect.bottom -20) and (j1.velx == 0 and j1.vely == 0):
                print('Colision Estatica')
                j1.rect.bottom = col.rect.top
                j1.vely = -10
                j1.salud -= 10
                j1.sonido.play()
            #Colision Enemigo Jugador
            elif (j1.rect.bottom > col.rect.bottom - 20 and j1.rect.bottom < col.rect.bottom + 20) and col.velx > 0:
                print("Colision Izquierda")
                j1.rect.right = col.rect.left +1
                j1.vely = -10
                j1.velx = 0
                j1.salud -= 10
                j1.sonido.play()
            elif (j1.rect.bottom > col.rect.bottom - 20 and j1.rect.bottom < col.rect.bottom + 20) and col.velx < 0:
                print('Colision Derecha')
                j1.rect.left = col.rect.right +1
                j1.vely = 10
                j1.velx = 0
                j1.salud -= 10
                j1.sonido.play()
            elif j1.rect.top < col.rect.bottom -20 and col.vely < 0:
                print('Colision Inferior')
                j1.rect.top = col.rect.bottom +1
                j1.velx = 10
                j1.vely = 0
                j1.salud -= 10
                j1.sonido.play()
            elif (j1.rect.bottom > col.rect.bottom -20 ) and col.vely > 0:
                print('Colision Superior')
                j1.rect.bottom = col.rect.bottom -21
                j1.velx = -10
                j1.vely = 0
                j1.salud -= 10
                j1.sonido.play()
            print(j1.salud)

        #Control del movimiento de los enemigos
        if random.randrange(100) > 90:
            if random.randrange(100) > 50:
                for e in enemigos:
                    if random.randrange(100) > 70:
                        e.velx = random.randrange(-5, 5)
                        e.vely = 0
            else:
                for e in enemigos:
                    if random.randrange(100) > 70:
                        e.vely = random.randrange(-5, 5)
                        e.velx = 0

        #Colisiones contra las cartas 
        lis_Col = pygame.sprite.spritecollide(j1, cartas, True)
        if len(lis_Col) > 0:
            j1.cartas +=1
            carta.sonido.play()
            print(j1.cartas)

        if j1.salud < 0:
            finJuego = True
            TXT = 'Lo lamento Has perdido'
            musica.stop()
            return finJuego, TXT, False
        if j1.cartas == 5:
            finJuego = False
            TXT = 'Feliciades Ganaste!!!'
            musica.stop()
            return finJuego, TXT, True

        #actualizar las clases
        jugadores.update()
        enemigos.update()
        
        #Pintar en Pantalla
        pantalla.fill(NEGRO)
        pantalla.blit(fondo, [f_posx,f_posy])
        jugadores.draw(pantalla)
        bloques.draw(pantalla)
        enemigos.draw(pantalla)
        cartas.draw(pantalla)
        fuente = pygame.font.Font(None, 30)
        texto = 'Salud: ' + str(j1.salud)
        texto_Cartas = str(j1.cartas) + '/5'
        txt_info = fuente.render(texto, True, ROJO)
        textoCartas = fuente.render(texto_Cartas, True, BLANCO)
        pantalla.blit(txt_info,[50,20])
        pantalla.blit(spriteCarta[9][4], [300,20])
        pantalla.blit(textoCartas, [320, 20])
        pygame.display.flip()
        reloj.tick(15)

        if j1.rect.right > lim_derecho:
            j1.rect.right = lim_derecho
            #SI EL USUARIO SOLICITA DESPLAZAR FONDO
            if f_posx > f_limite_der:
                f_posx += f_velx
                for b in bloques:
                    b.rect.x += f_velx
                for e in enemigos:
                    e.rect.x += f_velx
                for c in cartas:
                    c.rect.x += f_velx
        
        if j1.rect.left < lim_izquierdo:
            j1.rect.left = lim_izquierdo
            if f_posx < f_limite_izq:
                f_posx += -f_velx
                for b in bloques:
                    b.rect.x += -f_velx
                for e in enemigos:
                    e.rect.x += -f_velx
                for c in cartas:
                    c.rect.x += -f_velx

        if j1.rect.bottom > lim_abajo:
            j1.rect.bottom = lim_abajo
            #SI EL USUARIO SOLICITA DESPLAZAR FONDO
            if f_posy > f_limite_aba:
                f_posy += f_vely
                for b in bloques:
                    b.rect.y += f_vely
                for e in enemigos:
                    e.rect.y += f_vely
                for c in cartas:
                    c.rect.y += f_vely
        
        if j1.rect.top < lim_arriba:
            j1.rect.top = lim_arriba
            if f_posy < f_limite_arr:
                f_posy += -f_vely
                for b in bloques:
                    b.rect.y += -f_vely
                for e in enemigos:
                    e.rect.y += -f_vely
                for c in cartas:
                    c.rect.y += -f_vely
    pygame.quit()
