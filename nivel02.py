import pygame
import random
import configparser
from libJuego import *

BLANCO=[255,255,255]
VERDE=[0,255,0]
ROJO=[255,0,0]
AZUL=[0,0,255]
NEGRO=[0,0,0]
AMARILLO=[255,255,102]
AZUL_CLARO=[153,204,255]
VERDE_CLARO=[0,200,140]

ANCHO = 1080
ALTO = 720

class Jugador(pygame.sprite.Sprite):
    def __init__(self, mAnimation):
        pygame.sprite.Sprite.__init__(self)
        self.m = mAnimation
        self.con = 0
        self.dir = 0
        self.cteHasta = 0
        self.cteDesde = 0
        self.image=self.m[self.dir][self.con]
        self.rect = self.image.get_rect()
        self.rect.x = 100 #posicion x
        self.rect.y = 400
        self.velx = 0 #velocidad x
        self.vely = 0
        self.salud = 100
        self.golpe = False
        self.cartas = 0
        self.cont_salto = 0
        self.sonido = pygame.mixer.Sound('Sonidos/ough.ogg')
        self.bloques = pygame.sprite.Group()

    def gravedad(self, cte):
        if self.vely == 0:
            self.vely = 1
        else:
            self.vely += cte

    def update(self):
        self.rect.x += self.velx
        self.rect.y += self.vely

        if self.con < self.cteHasta:
            self.con +=1
        else:
            self.con = self.cteDesde 

        self.image=self.m[self.dir][self.con]

        self.gravedad(5)

        #COLISIONES CON BLOQUES
        lista_Colision = pygame.sprite.spritecollide(self, self.bloques, False)
        for b in lista_Colision:
            for b in lista_Colision:
                if self.vely < 0:
                    if self.rect.top < b.rect.bottom   and self.vely < 0:
                        self.rect.top = b.rect.bottom
                        self.vely = 0
                else:
                    if self.rect.bottom +1 > b.rect.top  and self.vely > 0:
                        self.rect.bottom = b.rect.top
                        self.vely = 0
                        self.cont_salto = 0

class Bloque(pygame.sprite.Sprite):
    def __init__(self, img, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        self.rect.x = pos[0] #posicion x
        self.rect.y = pos[1]

class Carta(pygame.sprite.Sprite):
    def __init__(self, img, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        self.rect.x = pos[0] #posicion x
        self.rect.y = pos[1]
        self.sonido = pygame.mixer.Sound('Sonidos/takeup.ogg')

class Bomba(pygame.sprite.Sprite):
    def __init__(self, img, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        self.rect.x = pos[0] #posicion x
        self.rect.y = pos[1]
        self.sonido = pygame.mixer.Sound('Sonidos/explosion.ogg')

class Perla(pygame.sprite.Sprite):
    def __init__(self, img, pos, vel):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        self.rect.x = pos[0] + 5 #posicion x
        self.rect.y = pos[1] + 25
        self.velx = vel
    
    def update(self):
        self.rect.x += self.velx

class EnemigoPerro(pygame.sprite.Sprite):
    def __init__(self, img, salud, pos):
        pygame.sprite.Sprite.__init__(self)
        self.m = img
        self.con = 0
        self.dir = 0
        self.cteHasta = 0
        self.cteDesde = 0
        self.image=self.m[self.dir][self.con]
        self.rect = self.image.get_rect()
        self.rect.x = pos[0] #posicion x
        self.rect.y = pos[1]
        self.velx = 0 #velocidad x
        self.vely = 0 #velocidad y
        self.salud = salud
        self.sonido = pygame.mixer.Sound('Sonidos/MuertePerro.ogg')
        self.bloques = pygame.sprite.Group()

    def gravedad(self, cte):
        if self.vely == 0:
            self.vely = 1
        else:
            self.vely += cte


    def update(self):
        self.rect.x += self.velx
        self.rect.y += self.vely

        if self.con < self.cteHasta:
            self.con +=1
        else:
            self.con = self.cteDesde
        if self.velx>0:
            self.dir=2
        if self.velx<0:
            self.dir=1

        if self.con < self.cteHasta:
            self.con += 1
        else:
            self.con = self.cteDesde

        self.image=self.m[self.dir][self.con]

        self.gravedad(1)

        #COLISIONES CON BLOQUES
        lista_Colision = pygame.sprite.spritecollide(self, self.bloques, False)
        for b in lista_Colision:
            if self.vely > 0:
                if self.rect.bottom > b.rect.top and self.vely > 0:
                    self.rect.bottom = b.rect.top
                    self.vely = 0

            self.velx = self.velx * -1

class Jefe(pygame.sprite.Sprite):
    def __init__(self, img, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]  #posicion x
        self.rect.y = pos[1]
        self.vely = -5
        self.velx = 0
        self.salud = 500
        self.sonido = pygame.mixer.Sound('Sonidos/damage_Boss.ogg')
    
    def update(self):
        self.velx += self.velx
        self.rect.y += self.vely

        if self.rect.bottom > ALTO-10:
            self.vely = -5
        if self.rect.top < 10:
            self.vely = 5

class Flama(pygame.sprite.Sprite):
    def __init__(self, img, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = img
        self.rect = self.image.get_rect()
        self.rect.x = pos[0] #posicion x
        self.rect.y = pos[1]
        self.velx = -5
        self.sonido = pygame.mixer.Sound('Sonidos/fire-ball.ogg')
    
    def update(self):
        self.rect.x += self.velx

def nivel02(pantalla):
    musica = pygame.mixer.Sound('Sonidos/ambiente2.ogg')
    musicaBoss = pygame.mixer.Sound('Sonidos/BossFight.ogg')
    musica.play(-1)
    musica.set_volume(0.3)
    info_mapa = configparser.ConfigParser()
    info_mapa.read('mapaJuego02.map')
    nom_archivo = info_mapa.get('informacion', 'archivo')

    #Definir los recortes colisionables
    fondo_Recortes = pygame.image.load(nom_archivo)
    info = fondo_Recortes.get_rect()
    fRecortes_ancho = info[2]
    fRecortes_alto = info[3]

    #Fondo del juego
    fondo = pygame.image.load('Imagenes/nivel02.png')
    info = fondo.get_rect()
    f_ancho = info[2]
    f_alto = info[3]

    numsp_ancho = int(info_mapa.get('informacion', 'sp_ancho'))
    numsp_alto = int(info_mapa.get('informacion', 'sp_alto'))
    ancho_sp = int(fRecortes_ancho / numsp_ancho)
    alto_sp = int(fRecortes_alto / numsp_alto)
    matriz = []
    
    for fil in range(numsp_alto):
        listAux = []

        for col in range(numsp_ancho):
            cuadro = fondo_Recortes.subsurface(ancho_sp*col,alto_sp*fil,ancho_sp,alto_sp)
            listAux.append(cuadro)

        matriz.append(listAux)
    
    mapa = info_mapa.get('informacion', 'mapa')
    filas = mapa.split('\n')

    lim_derecho = ANCHO - 250 
    lim_izquierdo = 100

    jugadores = pygame.sprite.Group()
    bloques = pygame.sprite.Group()
    perros = pygame.sprite.Group()
    cartas = pygame.sprite.Group()
    perlas = pygame.sprite.Group()
    jefes = pygame.sprite.Group()
    flamas = pygame.sprite.Group()
    bombas = pygame.sprite.Group()

    con_fil = 0
    for f in filas:
        con_col = 0
        for col in f:
            if col != '.':
                num_col = int(info_mapa.get(col, 'col'))
                num_fil = int(info_mapa.get(col, 'fil'))
                b=Bloque(matriz[num_fil][num_col], [ancho_sp*con_col, alto_sp*con_fil])
                bloques.add(b)
            con_col += 1
        con_fil += 1

    m = recortar('Imagenes/kate.png', [4, 4])
    j1 = Jugador(m)
    j1.bloques = bloques

    jugadores.add(j1)
 
    matrizModificadores = recortar('Imagenes/modificadores03.png', [16,16])
    matrizModificadores02 = recortar('Imagenes/modificadores04.png', [16,15])
    spriteJefe = recortar('Imagenes/alienJefe.png', [1,1])
    matrizPerros = recortar('Imagenes/Perros.png',[12,8])
    spriteCarta = matrizModificadores[9][4]
    spritePerla = matrizModificadores[12][9]
    spriteBomba = matrizModificadores02[13][10]
    
    pos_cartas = [[96, 143],[1280,640], [1248,79], [2112,495], [3072,79], [288,495]]
    for i in range(len(pos_cartas)):
        carta = Carta(spriteCarta, pos_cartas[i])
        cartas.add(carta)

    pos_perros = [[96, 143],[1280,640], [1248,79], [2212,495], [3072,79], [288,495]]
    for i in range(len(pos_perros)):
        perro = EnemigoPerro(matrizPerros, 80, pos_perros[i])
        perro.velx = 5
        perro.con = 9
        perro.dir = 5
        perro.cteDesde = 9
        perro.cteHasta = 12
        perro.bloques = bloques
        perros.add(perro)

    pos_bombas = [[992,328], [1600, 672],[1700, 672], [1952,480], [2432, 480]]
    for i in range(len(pos_bombas)):
        bomba = Bomba(spriteBomba, pos_bombas[i])
        bombas.add(bomba)

    jefe = Jefe(spriteJefe[0][0], [ ANCHO - 250, 100])
    jefes.add(jefe)

    f_posx = 0
    f_velx = -10

    f_limite_der = ANCHO - f_ancho + 50
    f_limite_izq = 110

    musicaBossPlay = False
    fin=False
    reloj = pygame.time.Clock()
    while (not fin):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin=True
                musica.stop()

            if  event.type == pygame.KEYDOWN:
                j1.golpe = False
                #DERECHA
                if event.key == pygame.K_RIGHT:
                    j1.velx = 10
                    j1.dir = 2
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  3
                #IZQUIERDA
                if event.key == pygame.K_LEFT:
                    j1.velx = -10
                    j1.dir = 1
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  3
                #ARRIBA
                if event.key == pygame.K_UP:
                    if j1.cont_salto < 2:
                        j1.vely = -30
                        j1.cont_salto += 1

                if event.key == pygame.K_z:
                    pos_perla = []
                    vel_perla = 0
                    if j1.dir == 1:
                        pos_perla = j1.rect.midtop
                        vel_perla =  -20
                    elif j1.dir == 2:
                        pos_perla = j1.rect.midtop
                        vel_perla = 20
                    elif j1.dir == 0:
                        break

                    p = Perla(spritePerla, pos_perla, vel_perla)
                    perlas.add(p)
                
                if event.key == pygame.K_ESCAPE:
                    musica.stop()
                    Pausa(pantalla)
                    musica.play(-1)
                
            if event.type == pygame.KEYUP:
                #DERECHA
                if event.key == pygame.K_RIGHT:
                    j1.velx = 0
                    j1.dir = 2
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  0
                #IZQUIERDA
                if event.key == pygame.K_LEFT:
                    j1.velx = 0
                    j1.dir = 1
                    j1.con = 0
                    j1.cteDesde = 0 
                    j1.cteHasta =  0
                #ARRIBA
                if event.key == pygame.K_UP:
                    #j1.vely = 0
                    pass 

        #Manejo de perlas y flamas
        for p in perlas:
            if  (p.rect.x < 0 or p.rect.x > ANCHO):
                perlas.remove(p)
                print('se elimino')
        for f in flamas:
            if  (f.rect.x < 0 or f.rect.x > ANCHO):
                flamas.remove(f)
                print('se elimino')
        
        #Manejo de colisiones de perlas con los perros
        for prr in perros:
            lista_colisiones_PerlaPerro = pygame.sprite.spritecollide(prr, perlas, True)
            if len(lista_colisiones_PerlaPerro) > 0:
                prr.salud -=10

            if prr.salud < 0:
                prr.sonido.play()
                perros.remove(prr)
        
        #Manejo de colisiones de Perlas contra las bombas
        for bm in bombas:
            lista_colisiones_PerlaBomba = pygame.sprite.spritecollide(bm, perlas, True)
            if len(lista_colisiones_PerlaBomba) > 0:
                bm.sonido.play()
                bombas.remove(bm)

        
        #Manejo de colisiones de Jugador con los perros
        lista_colisiones_J1Perro = pygame.sprite.spritecollide(j1, perros, False)   
        if len(lista_colisiones_J1Perro) > 0:
            j1.salud -=20
            j1.sonido.play()
            print(j1.salud)
            col = lista_colisiones_J1Perro[-1]
            if j1.rect.bottom > col.rect.top and j1.vely >= 0:
                j1.rect.bottom = col.rect.top
                j1.vely = -20
            if j1.rect.right > col.rect.left and col.velx < 0:
                j1.rect.right = col.rect.left
                j1.velx = -5
            elif j1.rect.left < col.rect.right and col.velx > 0:
                j1.rect.left = col.rect.right +5
                j1.velx = 5

        #Colisiones jugador contra las cartas 
        lis_Col = pygame.sprite.spritecollide(j1, cartas, True)
        if len(lis_Col) > 0:
            j1.cartas +=1
            carta.sonido.play()
            print(j1.cartas)
        
        #Manejo de colisiones jugador contra bombas 
        for bm in bombas:
            lis_col_J1Bombas = pygame.sprite.spritecollide(j1, bombas, True)
            if len(lis_col_J1Bombas) > 0:
                j1.salud -= 40
                j1.sonido.play()
                bm.sonido.play()
                if j1.rect.bottom > bm.rect.top and j1.rect.bottom < bm.rect.bottom:
                    j1.rect.bottom = bm.rect.top
                    j1.vely = -20

        #control de las flamas del jefe
        if f_posx <= f_limite_der + 10 and j1.cartas == len(pos_cartas):
            musica.stop()
            if musicaBossPlay == False:
                musicaBoss.play(-1)
                musicaBossPlay = True
            lis_Col_Flamas = pygame.sprite.spritecollide(j1, flamas, True)
            if len(lis_Col_Flamas) > 0:
                j1.salud -= 5
                j1.sonido.play()
            if random.randrange(100) > 92:
                pos_flama = [0,0]
                if random.randrange(100) > 80:
                    pos_flama = jefe.rect.midtop
                elif random.randrange(100) < 12:
                    pos_flama = jefe.rect.midbottom
                elif random.randrange(100) < 60 and random.randrange(100) > 40:
                    pos_flama = jefe.rect.midleft
                
                f = Flama(matrizModificadores[6][8], pos_flama)
                f.sonido.play()
                flamas.add(f)
            
            lista_colisiones_PerlaJefe = pygame.sprite.spritecollide(jefe, perlas, True)
            if len(lista_colisiones_PerlaJefe) > 0:
                jefe.salud -=5
                jefe.sonido.play()
                print('saludJefe: ', jefe.salud)
                if jefe.salud < 0:
                    jefes.remove(jefe)
                    musicaBoss.stop()

        #Condicion de perder
        if j1.salud < 0:
            finJuego = True
            TXT = 'Lo lamento Has perdido'
            musica.stop()
            musicaBoss.stop()
            return finJuego, TXT, False
        if j1.rect.top > ALTO:
            finJuego = True
            TXT = 'Lo lamento Has perdido'
            musica.stop()
            musicaBoss.stop()
            return finJuego, TXT, False

        #Condicion de ganar
        if jefe.salud < 0 and j1.cartas == len(pos_cartas):
            finJuego = False
            TXT = 'Feliciades Ganaste!!!'
            musica.stop()
            musicaBoss.stop()
            return finJuego, TXT, True

        #actualizar las clases
        jugadores.update()
        perros.update()
        perlas.update()
        jefe.update()
        flamas.update()

        #Pintar en Pantalla
        pantalla.fill(NEGRO)
        pantalla.blit(fondo, [f_posx,0])
        jugadores.draw(pantalla)
        bloques.draw(pantalla)
        if f_posx <= f_limite_der + 10 and j1.cartas == len(pos_cartas):
            jefes.draw(pantalla)
            flamas.draw(pantalla)
        perros.draw(pantalla)
        bombas.draw(pantalla)
        perlas.draw(pantalla)
        cartas.draw(pantalla)
        fuente = pygame.font.Font(None, 30)
        texto = 'Salud: ' + str(j1.salud)
        texto_Cartas = str(j1.cartas) + '/' + str(len(pos_cartas))
        txt_info = fuente.render(texto, True, ROJO)
        textoCartas = fuente.render(texto_Cartas, True, BLANCO)
        pygame.draw.rect(pantalla, NEGRO, [45, 19, 115, 22], 0)
        pantalla.blit(txt_info,[50,20])
        pantalla.blit(spriteCarta, [300,20])
        pantalla.blit(textoCartas, [326, 20])
        pygame.display.flip()
        reloj.tick(15)

        if j1.rect.right > lim_derecho:
            j1.rect.right = lim_derecho
            #SI EL USUARIO SOLICITA DESPLAZAR FONDO
            if f_posx > f_limite_der:
                print(f_posx, f_limite_der)
                if f_posx >= f_ancho:
                    f_velx = f_velx * 1.5
                    print('aumento')
                f_posx += f_velx
                for b in bloques:
                    b.rect.x += f_velx
                for e in perros:
                    e.rect.x += f_velx
                for c in cartas:
                    c.rect.x += f_velx
                for bm in bombas:
                    bm.rect.x += f_velx

        if j1.rect.left < lim_izquierdo:
            j1.rect.left = lim_izquierdo
            if f_posx < f_limite_izq:
                f_posx += -f_velx
                for b in bloques:
                    b.rect.x += -f_velx
                for e in perros:
                    e.rect.x += -f_velx
                for c in cartas:
                    c.rect.x += -f_velx
                for bm in bombas:
                    bm.rect.x += -f_velx

    pygame.quit()
